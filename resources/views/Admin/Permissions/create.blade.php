@extends('layouts.admin')
@section('content')
<br>
<form action="/admin/permissions/store" method="post">
@csrf
    <div class="form-group">
    <label for="title" class="h4">@lang('admin.permission_title')</label>
    <input type="text" id="title" class="form-control" placeholder='@lang('admin.permission_title_placeholder')' name="title">
    <br>
    <label for="title_ar" class="h4">@lang('admin.permission_title_ar')</label>
    <input type="text" id="title_ar" class="form-control" placeholder='@lang('admin.permission_title_placeholder_ar')' name="title_ar">
  </div>
  <table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">@lang('admin.permission')</th>
      <th scope="col">@lang('admin.state')</th>
    </tr>
  </thead>
  <tbody>
    <tr>
     <td><label class="checkbox-inline">@lang('admin.view')</label></td>
     <td><input type="checkbox" checked data-toggle="toggle" name="view"></td>
    </tr>
    <tr>
        <td><label class="checkbox-inline">@lang('admin.edit')</label></td>
        <td><input type="checkbox" checked data-toggle="toggle" name="edit"> </td>
    </tr>
    <tr>
        <td><label class="checkbox-inline">@lang('admin.add')</label></td>
        <td><input type="checkbox" checked data-toggle="toggle" name="add"></td>
    </tr>
    <tr>
        <td><label class="checkbox-inline">@lang('admin.delete')</label></td>
        <td><input type="checkbox" checked data-toggle="toggle" name="delete"></td>
    </tr>
    <tr>
        <td><label class="checkbox-inline">@lang('admin.permission_access')</label></td>
        <td><input type="checkbox" checked data-toggle="toggle" name="permissions"></td>
    </tr>
    <tr>
        <td><label class="checkbox-inline">@lang('admin.analytics_access')</label></td>
        <td><input type="checkbox" checked data-toggle="toggle" name="analytics"></td>
    </tr>
    <tr>
        <td><label class="checkbox-inline">@lang('admin.services_access')</label></td>
        <td><input type="checkbox" checked data-toggle="toggle" name="services"></td>
    </tr>
    <tr>
        <td><label class="checkbox-inline">@lang('admin.clothes_access')</label></td>
        <td><input type="checkbox" checked data-toggle="toggle" name="clothes"></td>
    </tr>
    <tr>
        <td><label class="checkbox-inline">@lang('admin.storage_access')</label></td>
        <td><input type="checkbox" checked data-toggle="toggle" name="storage"></td>
    </tr>
  </tbody>
</table>
  <br>
  <button type="submit" class="btn btn-primary">@lang('admin.submit')</button>
</form>
@endsection

