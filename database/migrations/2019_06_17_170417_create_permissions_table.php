<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('title_ar');
            $table->string('view_status')->nullable();
            $table->string('edit_status')->nullable();
            $table->string('add_status')->nullable();
            $table->string('delete_status')->nullable();
            $table->string('analytics_status')->nullable();
            $table->string('services_status')->nullable();
            $table->string('storage_status')->nullable();
            $table->string('clothes_status')->nullable();
            $table->string('permissions_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prmissions');
    }
}
