@extends('layouts.Http_errors')
@section('title')
403
@endsection

@section('error')
403
@endsection

@section('message')
Access Denied You Are Not Allowed To Enter This Path
@endsection

@section('go_home')
<br>
<a href="javascript:history.back()" class='btn btn-info'>Back</a>
<a href="/user" class='btn btn-info'>Home</a>

@endsection