@extends('layouts.admin')
@section('content')
<a href="/admin/articles/create" class="btn btn-primary">@lang('admin.add_new_article')</a>
<br>
<br>
<div class="card">
<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">@lang('admin.articles')</th>
      <th scope="col">@lang('admin.options')</th>
    </tr>
  </thead>
  <tbody>
<!--   put your variables here later --> 
@foreach ($articles as $article)
      <tr>
      @if( Config::get('app.locale') == 'en')
      <td>{{$article->article_name}}</td>
      @else
      <td>{{$article->article_name_ar}}</td>
      @endif
      <td>
      {!! Form::open(array('url' => '/admin/articles/destroy/','method' => 'Delete')) !!}
      <a href="/admin/articles/edit/" class="btn btn-success">@lang('admin.edit')</a> 
      {!! Form::submit(trans('admin.delete'), ['class' => 'btn btn-danger']) !!}
      {!! Form::close() !!}
     </td>
    </tr>
@endforeach
    </tbody>
</table>
</div>
@endsection