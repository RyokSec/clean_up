@extends('layouts.admin')
@section('content')
<br>
<form action="/admin/permissions/users/store/" method="post">
@csrf

<div>
<label>@lang('admin.choose_user')</label>
<br>
<select name="user" class="form-control">
  @foreach($users as $user)
    <option>{{$user->name}}</option>
    @endforeach
  </select>
</div>
<br>
<div>
<label>@lang('admin.choose_permission')</label>
<br>
  <select name="permission" class="form-control">
   @foreach($permissions as $permission)
   @if( Config::get('app.locale') == 'en')
    <option>{{$permission->title}}</option>
    @else
    <option>{{$permission->title_ar}}</option>
    @endif
    @endforeach
  </select>
</div>
  <br>
  <button type="submit" class="btn btn-primary">@lang('admin.assign')</button>
</form>
@endsection


