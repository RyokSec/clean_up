<?php

return[
    //general 
    'delete' => 'Delete' , 
    'edit' => 'Edit',
    'admin' => 'Admin',
    'submit' => 'submit',
    //nav
    'logout' => 'Logout',
    'general' => 'General',
    'dashboard' => 'Dashboard',
    'services' => 'Services',
    'analytics' => 'Analytics',
    'permissions' => 'Permissions And Users',
    'profile' => 'Profile',
    'general_settings' => 'General Settings',
    'dir' => '',
    //Permissions
    'add_new_permission' => 'Add New Permission',
    'permission_title' => 'Title',
    'options' => 'Options',
    'assign_permission' => 'Assign Permission To User',
    'create_&_assign' => 'Create New User And Assign Permission',
    'permission' => 'Permssion',
    //Create new permission page
    'permission_title_ar' => 'Permission Title in Arabic' ,
    'permission_title' => 'Permission Title in English', 
    'permission_title_placeholder' => 'Enter Permission Title in English',
    'permission_title_placeholder_ar' => 'Enter Permission Title in Arabic',
    'state' => 'State' , 
    'view' => 'View',
    'edit' => 'Edit',
    'add' => 'Add',
    'delete' => 'Delete' ,
    'permission_access' => 'Permissions Access',
    'analytics_access' => 'Analytics Access',
    'services_access' => 'Services Access',
    'clothes_access' => 'Clothes Access',
    'storage_access' => 'Storage Access' ,
    //Assign permission page
    'assign' => 'Assign',
    'choose_user' => 'Choose User To Assign Permission',
    'choose_permission' => 'Choose Permission To Assign',
    'user' => 'User',
    //articles
    'articles' => 'Articles' ,
    'add_new_article' => 'Add New Article',
    'article_title' => 'Article Name En',
    'article_title_ar' => 'Article Name Ar',
];