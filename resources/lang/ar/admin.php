<?php

return[
    //general 
    'delete' => 'حذف' , 
    'edit' => 'تعديل',
    'admin' => 'المشرف',
    'submit' => 'حفظ',
    //nav
    'logout' => 'تسجيل الخروج',
    'general' => 'عام',
    'dashboard' => 'لوحة التحكم',
    'services' => 'الخدمات',
    'analytics' => 'احصائيات',
    'permissions' => 'الصلاحيات ',
    'profile' => 'حسابي',
    'general_settings' => 'اعدادات الموقع العامة',
    'dir' => 'rtl',
    //Permissions
    'add_new_permission' => 'اضف صلاحية جديدة',
    'permission' => 'الصلاحية',
    'options' => 'الخيارات',
    'assign_permission' => 'تعيين صلاحية للمستخدم',
    'create_&_assign' => 'انشاء مستخدم و تعيين صلاحية له',
    //create new permission page
    'permission_title' => 'مسمى الصلاحية باللغة الانغليزية' ,
    'permission_title_ar' => 'مسمى الصلاحية باللغة العربية' ,
    'permission_title_placeholder' => 'ادخل مسمى الصلاحية باللغة الانغليزية', 
    'permission_title_placeholder_ar' => 'ادخل مسمى الصلاحية باللغة العربية',
    'state' => 'الحالة' , 
    'view' => 'الاطلاع',
    'edit' => 'التعديل',
    'add' => 'الاضافة',
    'delete' => 'الحذف' ,
    'permission_access' => 'الدخول لقسم الصلاحيات',
    'analytics_access' => 'الدخول لقسم الاحصائيات',
    'services_access' => 'الدخول لقسم الخدمات',
    'clothes_access' => 'الدخول لقسم الملابس',
    'storage_access' => 'الدخول لقسم المخزن',
    //Assign permission page
    'assign' => 'تعيين',
    'choose_user' => ' اختر مستخدم لتعيين الصلاحية له', 
    'choose_permission' => 'اختر الصلاحية لتعيينها للمستخدم',
    'user' => 'المستخدم',
    //articles
    'articles' => 'القطع',
    'add_new_article' => 'اضف قطعة جديدة',
    'article_title' => 'مسمى القطعة باللغة الانغليزية',
    'article_title_ar' => 'مسمى القطعة باللغة العربية',
];