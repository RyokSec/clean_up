@extends('layouts.admin')
@section('content')
@if(count($permissions) > 0)
<br>
<a href="/admin/permissions/new" class="btn btn-primary">@lang('admin.add_new_permission')</a>
<br>
<br>
<div class="card">
<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">@lang('admin.permission')</th>
      <th scope="col">@lang('admin.options')</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      @foreach($permissions as $permission)
      @if( Config::get('app.locale') == 'en')
      <td>{{$permission->title}}</td>
      @else
      <td>{{$permission->title_ar}}</td>
      @endif
      <td>
      {!! Form::open(array('url' => '/admin/permission/delete/'.$permission->id,'method' => 'Delete')) !!}
      <a href="/admin/permission/edit/{{$permission->id}}" class="btn btn-success">@lang('admin.edit')</a> 
      {!! Form::submit(trans('admin.delete'), ['class' => 'btn btn-danger']) !!}
      {!! Form::close() !!}
     </td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>
@else
<a href="/admin/permissions/new" class="btn btn-danger">@lang('admin.add_new_permission')</a>
<br>
<br>
<h3>No Permissions Have Been added</h3>
@endif
<hr>
<br>
<br>
<br>
@if(count($users_with_permissions) > 0)
<a href="/admin/permissions/users/assign" class="btn btn-success mb-1">@lang('admin.assign_permission')</a>
<a href="/admin/permissions/users/new" class="btn btn-primary">@lang('admin.create_&_assign')</a>
<br>
<br>
<div class="card">
<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">@lang('admin.admin')</th>
      <th scope="col">@lang('admin.permission')</th>
      <th scope="col">@lang('admin.options')</th>
    </tr>
  </thead>
  <tbody>
  @foreach($users_with_permissions as $user_with_permission)
    <tr>
      <td>{{$user_with_permission->name}}</td>
      @if( Config::get('app.locale') == 'en')
      <td>{{$user_with_permission->permissionsGroup->title}}</td>
      @else
      <td>{{$user_with_permission->permissionsGroup->title_ar}}</td>
      @endif
     <td>{!! Form::open(array('url' => '/admin/permission/users/delete/'.$user_with_permission->id,'method' => 'Delete')) !!}
      <a href="/admin/permission/users/edit/{{$user_with_permission->id}}" class="btn btn-success">@lang('admin.edit')</a> 
      {!! Form::submit(trans('admin.delete'), ['class' => 'btn btn-danger']) !!}
      {!! Form::close() !!}
     </td>
    </tr>
  @endforeach  
  </tbody>
</table>
</div>
@elseif(count($permissions) > 0)
<a href="/admin/permissions/users/assign" class="btn btn-success">@lang('admin.assign_permission')</a>
<br>
<br>
<h3>No Users With Permissions At The Momment</h2>
@endif
@endsection