<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use APP\User;
use App\Permissions;
use Auth;
use App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class PermissionsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');     
    }

    public function index()
    {
        //check permissions
            if (!@Auth::user()->is_admin){
            return redirect('/user');
        }
        //check view permission and permission route states
        if(@Auth::user()->permissionsGroup->add_status == 'on' && @Auth::user()->permissionsGroup->permissions_status == 'on'){
        $permissions = Permissions::all();
        $users_with_permissions = User::whereNotNull('permissions_id')->get();
        $title = 'Permissions View';
        $title_ar = 'الصلاحيات';
        return view('Admin.Permissions.Permissions', compact('permissions' , 'title' , 'users_with_permissions' , 'title_ar'));
        }
        abort(403, 'Unauthorized action.');
    }

    public function create()
    {
         //check permissions
            if (!@Auth::user()->is_admin){
            return redirect('/user');
         }
        //check add permission and permission route states
        if(@Auth::user()->permissionsGroup->add_status == 'on' && @Auth::user()->permissionsGroup->permissions_status == 'on'){
         $title = 'Add New Permission';
         $title_ar = 'انشاء صلاحية جديدة';
        return view('Admin.Permissions.create' , compact('title', 'title_ar'));
    }
    abort(403, 'Unauthorized action.');
    }

    public function store(Request $request)
    {
        //check permissions
        if (!@Auth::user()->is_admin){
            return redirect('/user');
        }
         //check add permission and permission route states
        if(@Auth::user()->permissionsGroup->add_status == 'on' &&
        @Auth::user()->permissionsGroup->permissions_status == 'on'){
         //validator 
        $request->validate([
            'title' => 'required',
            'title_ar' => 'required',
        ]);
        //store process
        $permission = new Permissions ;
        $permission->title = $request->input('title');
        $permission->title_ar = $request->input('title_ar');
        $permission->view_status = $request->input('view');
        $permission->add_status = $request->input('add');
        $permission->edit_status = $request->input('edit');
        $permission->delete_status = $request->input('delete');
        $permission->permissions_status = $request->input('permissions');
        $permission->analytics_status = $request->input('analytics');
        $permission->services_status = $request->input('services');
        $permission->storage_status = $request->input('storage');
        $permission->clothes_status = $request->input('clothes');
        $permission->save();
        //redirect
        $locale = App::getLocale();
      if($locale == 'en'){
        return redirect()->route('Permissions')->with('success' , 'Permission Added Successfully');
      }
      return redirect()->route('Permissions')->with('success' , 'تمت اضافة الصلاحية بنجاح');
    }
    abort(403, 'Unauthorized action.');
    }

        public function edit($id)
        {
        //check permissions
        if (!@Auth::user()->is_admin){
            return redirect('/user');
        }
          //check edit permission and permission route states
        if(@Auth::user()->permissionsGroup->edit_status == 'on' &&
        @Auth::user()->permissionsGroup->permissions_status == 'on'){
        $title = 'Edit Permission';
        $title_ar = 'تعديل الصلاحية';
        $data = Permissions::find($id);
        return view('Admin.Permissions.edit' , compact('data' , 'title' , 'title_ar'));
    }
    abort(403, 'Unauthorized action.');
     }

    public function update(Request $request , $id)
    {
        //check permissions
        if (!@Auth::user()->is_admin){
            return redirect('/user');
        }
          //check edit permission and permission route states
        if(@Auth::user()->permissionsGroup->edit_status == 'on' &&
        @Auth::user()->permissionsGroup->permissions_status == 'on'){
        //store process
        $permission = Permissions::find($id) ;
        $permission->title = $request->input('title');
        $permission->title_ar = $request->input('title_ar');
        $permission->view_status = $request->input('view');
        $permission->add_status = $request->input('add');
        $permission->edit_status = $request->input('edit');
        $permission->delete_status = $request->input('delete');
        $permission->permissions_status = $request->input('permissions');
        $permission->analytics_status = $request->input('analytics');
        $permission->services_status = $request->input('services');
        $permission->storage_status = $request->input('storage');
        $permission->clothes_status = $request->input('clothes');
        $permission->save();
        //redirect
        $locale = App::getLocale();
        if($locale == 'en'){
            return redirect()->route('Permissions')->with('success' , 'Permission Updated Successfully');
        }
        return redirect()->route('Permissions')->with('success' , 'تم تعديل الصلاحية بنجاح');
    }
    abort(403, 'Unauthorized action.');
    }
    public function destroy($id)
    {
         //check permissions
         if (!@Auth::user()->is_admin){
          return redirect('/user');
          }
         //check delete permission and permission route states
        if(@Auth::user()->permissionsGroup->delete_status == 'on' &&
         @Auth::user()->permissionsGroup->permissions_status == 'on'){
        //removing its id from all attached users
        $users_to_overwrite = User::where('permissions_id', $id)->get();
        foreach($users_to_overwrite as $users){
            $users = User::find($users->id);
            $users->permissions_id = null;
            $users->is_admin = null ;
            $users->save();
        }
        //delete the permission
        $Destroy = Permissions::find($id);
        $Destroy->delete();
        $locale = App::getLocale();
        if($locale == 'en'){
        return redirect()->route('Permissions')->with('success' , 'Permission Deleted successfully');
        }
        return redirect()->route('Permissions')->with('success' , 'تم حذف الصلاحية بنجاح');
    }
    abort(403, 'Unauthorized action.');
    }

    public function assign()
    {
         //check permissions
        if (!@Auth::user()->is_admin){
         return redirect('/user');
        }
        //check add permission and permission route states
         if(@Auth::user()->permissionsGroup->add_status == 'on' &&
         @Auth::user()->permissionsGroup->permissions_status == 'on'){         
        $title = 'Assign Permission';
        $title_ar = 'تعيين صلاحية' ; 
        $users = User::all();
        $permissions = Permissions::all();
        return view('Admin.Permissions.assign_permission.assign' , compact('title' , 'users' , 'permissions' , 'title_ar'));
    }
    abort(403, 'Unauthorized action.');   
    }

    public function store_permissiongroup(Request $request)
    {
         //check permissions
         if (!@Auth::user()->is_admin){
        return redirect('/user');
        }
        //check store permission and permission route states
        if(@Auth::user()->permissionsGroup->add_status == 'on' &&
        @Auth::user()->permissionsGroup->permissions_status == 'on'){  
        //validator 
        $request->validate([
            'user' => 'required',
            'permission' => 'required'
        ]);
            //getting user and permission id based on the name 
        $user_name = DB::table('users')->where('name', $request->input('user'))->first();
        $user_id = $user_name->id;
        $permission_name =  DB::table('permissions')->where('title', $request->input('permission'))->first();
        $permission_id = $permission_name->id;
        //stroing the permission id in the user table 
        $user_relationship = User::find($user_id);
        $user_relationship->permissions_id = $permission_id;
        $user_relationship->is_admin = 1 ;
        $user_relationship->save();
        //redirect
        $locale = App::getLocale();
        if($locale == 'en'){
            return redirect()->route('Permissions')->with('success' , 'Permission Assigned Successfully');
        }
        return redirect()->route('Permissions')->with('success' , 'تم تعيين الصلاحية بنجاح');
    }
    abort(403, 'Unauthorized action.');
    }

    public function edit_permissiongroup($user_id)
    {
        //admin permission check
        if (!@Auth::user()->is_admin){
            return redirect('/user');
        }
        //edit permission and permissions route access check
        if(@Auth::user()->permissionsGroup->edit_status == 'on' && 
        @Auth::user()->permissionsGroup->permissions_status == 'on'){
        $user = User::find($user_id);
        $permissions = Permissions::all();
        $title = 'Edit User Permission';
        $title_ar = 'تعديل صلاحية المستخدم';
        return view('Admin.Permissions.assign_permission.edit' , compact('user' , 'permissions','title' , 'title_ar'));
    }
        abort(403, 'Unauthorized action.');
    }


    public function update_permissiongroup(Request $request , $user_id)
    {

         //check permissions
         if (!@Auth::user()->is_admin){
        return redirect('/user');
        }
        //check update permission and permission route states
        if(@Auth::user()->permissionsGroup->edit_status == 'on' &&
        @Auth::user()->permissionsGroup->permissions_status == 'on'){ 
        
            //validator 
        $request->validate([
            'permission' => 'required'
        ]);
        $user = User::find($user_id);
        $locale = App::getLocale();
        //english 
        if($locale == 'en'){
            $permission_title = $request->input('permission');
            $permissions_id = Permissions::where('title' ,$permission_title)->get();
            $user->permissions_id = $permissions_id[0]->id;
            $user->save();
            return redirect()->route('Permissions')->with('success' , 'User Permission Edited Successfully');
        }
        //getting arabic title then taking id from it 
        $permission_title = $request->input('permission');
        $permissions_id = Permissions::where('title_ar' ,$permission_title)->get();
        $user->permissions_id = $permissions_id[0]->id;
        $user->save();
        return redirect()->route('Permissions')->with('success' , 'تم تعديل صلاحية المستخدم بنجاح');
    }
    abort(403, 'Unauthorized action.');
    }

        public function destroy_permissiongroup($id)
    {
        //check permissions
        if (!@Auth::user()->is_admin){
        return redirect('/user');
        }
        //check delete permission and permission route states
        if(@Auth::user()->permissionsGroup->delete_status == 'on' &&
        @Auth::user()->permissionsGroup->permissions_status == 'on'){ 
                    
        $user = User::find($id);
        $user->permissions_id = null;
        $user->is_admin = null ;
        $user->save();
        $locale = App::getLocale();
        if($locale == 'en'){
        return redirect()->route('Permissions')->with('success' , 'Permission Removed Successfully');
        }
        return redirect()->route('Permissions')->with('success' , 'تم ازالة الصلاحية من المستخدم بنجاح');
    }
    abort(403, 'Unauthorized action.');
    }

    public function new_user()
    {
                //check permissions
        if (!@Auth::user()->is_admin){
        return redirect('/user');
        }
        //check delete permission and permission route states
        if(@Auth::user()->permissionsGroup->add_status == 'on' &&
        @Auth::user()->permissionsGroup->permissions_status == 'on'){ 
            $permissions = Permissions::all();      
        return view('Admin.Permissions.Create_new_user.create', compact('permissions'));
    }
    abort(403, 'Unauthorized action.');
    }

    public function store_new_user(Request $request)
    {
         //check permissions
        if (!@Auth::user()->is_admin){
        return redirect('/user');
        }
        //check delete permission and permission route states
        if(@Auth::user()->permissionsGroup->add_status == 'on' &&
        @Auth::user()->permissionsGroup->permissions_status == 'on'){ 
         //validator 
        $request->validate([
            'name' => ['required', 'string', 'max:255', 'unique:users'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'image' => ['required' , 'image' , 'mimes:jpeg,png,jpg', 'max:2048'],
            'image' => 'required|image',

        ]);
        $user = new User;
        $user->name = $request->input('name');
        $user->is_admin = 1;
        $user->email = $request->input('email');
        $permissionid = Permissions::where('title' , $request->input('permission'))->get();
        $user->permissions_id = $permissionid[0]->id;
        $user->password = Hash::make($request->input('password'));
        $imageName = time().'.'.request()->image->getClientOriginalExtension();
        $image_path = '/userpicture/'.$imageName;
        request()->image->move(public_path('/userpicture'), $imageName);  
        $user->image = $image_path;
        $user->save();
        //redirect
        $locale = App::getLocale();
        if($locale == 'en'){
        return redirect()->route('Permissions')->with('success', 'Admin Created Successfully');
        }
        return redirect()->route('Permissions')->with('success', 'تم انشاء حساب المشرف بنجاح');
    }
    abort(403, 'Unauthorized action.');
    }
}