@extends('layouts.admin')
@section('content')
<br>
{!! Form::model($user,['method' => 'PATCH','action'=>['PermissionsController@update_permissiongroup','id'=>$user->id]]) !!}
@csrf
<div>
<label>@lang('admin.user')</label>
<input type="text" name='user' class='form-control' readonly value='{{$user->name}}'>
</div>
<br>
<div>
<label>@lang('admin.choose_permission')</label>
  <select name="permission"class="form-control">
   @foreach($permissions as $permission)

   @if( Config::get('app.locale') == 'en')
    <option>{{$permission->title}}</option>
    @else
    <option>{{$permission->title_ar}}</option>
    @endif

    @endforeach
  </select>
</div>
  <br>
  {{Form::submit(trans('admin.assign'),['class' => 'btn btn-primary'])}}
@endsection

