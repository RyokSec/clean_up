@extends('layouts.admin')
@section('script')
<script src="{{ asset('js/article_create.js') }}" ></script>
@endsection
@section('content')
<div class="form-group col-md-10">

  <form action="/admin/articles/store" method="post" enctype="multipart/form-data">
  @csrf
    <label for="title" class="h4">@lang('admin.article_title')</label>
    <input type="text" id="title" name="article_name" class="form-control" placeholder='@lang('admin.article_title')'>
    <br>
    <label for="title_ar" class="h4">@lang('admin.article_title_ar')</label>
    <input type="text" id="title_ar"  name="article_name_ar" class="form-control" placeholder='@lang('admin.article_title_ar')'>
    <br>
    <div class="form-group">
      <h1 class="font-weight-light text-center text-lg-left mt-4 mb-0">Choose Icon</h1>

<hr class="mt-2 mb-5">

<div class="row text-center text-lg-left">

  <div class="col-lg-3 col-md-4 col-6 text-center">
          <img class="img-fluid img-thumbnail" src="/Articles/shirt.png" alt="shirt">
          <span style="font-family:italic; font-decoration:none; color:black;">Shirt</span>
  <input type="radio" name="icon" onchange="get_selected_radio()"  value="shirt" class="mb-4">
  </div>
  
  <div class="col-lg-3 col-md-4 col-6 text-center">
          <img class="img-fluid img-thumbnail" src="/Articles/hood.png" alt="hood">
          <span style="font-family:italic; font-decoration:none; color:black;">Hood</span>
  <input type="radio" name="icon" onchange="get_selected_radio()"  value="hood" class="mb-4">
  </div>

  <div class="col-lg-3 col-md-4 col-6 text-center text-center">
          <img class="img-fluid img-thumbnail" src="/Articles/suit.png" alt="suit">
          <span style="font-family:italic; font-decoration:none; color:black;">Suit</span>
  <input type="radio" name="icon" onchange="get_selected_radio()"  value="suit" class="mb-4">
  </div>
  <div class="col-lg-3 col-md-4 col-6 text-center">
          <img class="img-fluid img-thumbnail" src="/Articles/necktie.png" alt="tie">
          <span style="font-family:italic; font-decoration:none; color:black;">Necktie</span>
  <input type="radio" name="icon" onchange="get_selected_radio()"  value="necktie" class="mb-4">
  </div>
  <div class="col-lg-3 col-md-4 col-6 text-center">
          <img class="img-fluid img-thumbnail" src="/Articles/skirt.png" alt="skirt">
          <span style="font-family:italic; font-decoration:none; color:black;">Skirt</span>
  <input type="radio" name="icon" onchange="get_selected_radio()"  value="skirt" class="mb-4">
  </div>
  <div class="col-lg-3 col-md-4 col-6 text-center">
          <img class="img-fluid img-thumbnail" src="/Articles/trousers.png" alt="trousers">
          <span style="font-family:italic; font-decoration:none; color:black;">Trousers</span>
  <input type="radio" name="icon" onchange="get_selected_radio()"  value="trousers" class="mb-4">
  </div>
  <div class="col-lg-3 col-md-4 col-6 text-center">
          <img class="img-fluid img-thumbnail" src="/Articles/jacket.png" alt="jacket">
          <span style="font-family:italic; font-decoration:none; color:black;">Jacket</span>
         <input type="radio" name="icon" onchange="get_selected_radio()"  value="jacket" class="mb-4">
  </div>
  <div class="col-lg-3 col-md-4 col-6 text-center">
     <img class="img-fluid img-thumbnail" src="/Articles/dress.png" alt="dress">
     <span style="font-family:italic; font-decoration:none; color:black;">Dress</span>
  <input type="radio" name="icon" onchange="get_selected_radio()"  value="dress" class="mb-4">
  </div>
</div>
      <h3><span><input type="radio" name="icon" onchange="get_selected_radio()" value="image" id="image_radio"></span>
        or upload your own icon ..
      </h3>

      <div class="custom-file mb-3 col-md-8" id="file_input">
        <input type="file" class="custom-file-input" id="image" name="image" onchange="get_filename()">
        <label class="custom-file-label" for="customFile" id="pic_label">choose file</label>
      </div>
      <br>
    <input type="submit" class="btn btn-primary ml-1" value="Save">
  </div>
  </form>

  @endsection

  