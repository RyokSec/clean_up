@extends('layouts.Http_errors')
@section('title')
404
@endsection

@section('error')
404
@endsection

@section('message')
Looks like the page you were looking for is no longer here.
@endsection

@section('go_home')
<br>
<a href="javascript:history.back()" class='btn btn-info'>Back</a>
<a href="/user" class='btn btn-info'>Home</a>

@endsection