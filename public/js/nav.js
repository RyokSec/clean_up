jQuery(function ($) {

    $(".sidebar-dropdown > a").click(function () {
        $(".sidebar-submenu").slideUp(200);
        if ($(this).parent().hasClass("active")) {
            $(".sidebar-dropdown").removeClass("active");
            $(this).parent().removeClass("active");
        } else {
            $(".sidebar-dropdown").removeClass("active");
            $(this).next(".sidebar-submenu").slideDown(200);
            $(this).parent().addClass("active");
        }

    });
});

function responsive_nav(x) {
    if (x.matches) { // If media query matches
      document.getElementById('nav').classList.add('toggled');
    } else {
        document.getElementById('nav').classList.remove('toggled');
    }
  }
  
  var x = window.matchMedia("(min-width: 760px)")
  responsive_nav(x) // Call listener function at run time
  x.addListener(responsive_nav) // Attach listener function on state changes

  function collapse_nav(){
      document.getElementById('nav').classList.remove('toggled');
  }
  
