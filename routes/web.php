<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//lang
Route::get('locale/{locale}', function($locale){
    Session::put('locale' , $locale);
    return redirect()->back();
});


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/user', 'HomeController@index')->name('User Dashboard');

//admin route
Route::get('/admin', 'AdminController@Dashboard')->name('Admin Dashboard');
//permissions routes
Route::get('/admin/permissions', 'PermissionsController@index')->name('Permissions');
Route::get('/admin/permissions/new', 'PermissionsController@create')->name('Add Permission');
Route::post('/admin/permissions/store', 'PermissionsController@store')->name('Store Permission');
Route::get('/admin/permission/edit/{id}', 'PermissionsController@edit')->name('Edit Permission');
Route::match(['put', 'patch'], '/admin/permission/update/{id}','PermissionsController@update')->name('Update Permission');
Route::DELETE('/admin/permission/delete/{id}', 'PermissionsController@destroy')->name('Destroy Permission');
//assign permission to a user
Route::get('/admin/permissions/users/assign', 'PermissionsController@assign')->name('assign');
Route::post('/admin/permissions/users/store', 'PermissionsController@store_permissiongroup')->name('Store PermissionGroup');
Route::get('/admin/permission/users/edit/{id}', 'PermissionsController@edit_permissiongroup')->name('Edit PermissionGroup');
Route::match(['put', 'patch'], '/admin/permission/users/update/{id}','PermissionsController@update_permissiongroup')->name('Update PermissionGroup');
Route::DELETE('/admin/permission/users/delete/{id}', 'PermissionsController@destroy_permissiongroup')->name('Destroy PermissionGroup');
//create new user and assign role by admin
Route::get('/admin/permissions/users/new', 'PermissionsController@new_user')->name('new user');
Route::post('/admin/permissions/users/rigister/', 'PermissionsController@store_new_user')->name('Store new user');

//clothes routes
Route::get('/admin/articles', 'ArticlesController@index')->name('Articles');
Route::get('/admin/articles/create', 'ArticlesController@create')->name('create new article');
Route::post('/admin/articles/store', 'ArticlesController@store')->name('store new article');
Route::get('/admin/articles/edit/{id}', 'ArticlesController@edit')->name('edit article');
Route::match(['put', 'patch'] ,'/admin/articles/update/{id}', 'ArticlesController@update')->name('update article');
Route::DELETE('/admin/articles/destroy/{id}', 'ArticlesController@destroy')->name('delete article');


