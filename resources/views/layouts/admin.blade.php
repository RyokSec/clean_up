<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('favicon.jpg') }}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @if( Config::get('app.locale') == 'en')
    <title>{{$title}}</title>
    @else
    <title>{{$title_ar}}</title>
    @endif
    <!-- Scripts -->
    @yield('script')
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="{{ asset('js/admin.js') }}" defer></script>
    <script src="{{ asset('js/nav.js') }}" defer></script>
    <script src="{{ asset('js/bootstrap2-toggle.min.js') }}" ></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    @if( Config::get('app.locale') == 'en')
    <link href="{{ asset('css/nav.css') }}" rel="stylesheet">
    @else
    <link href="{{ asset('css/nav_ar.css') }}" rel="stylesheet">
    @endif
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="{{asset ('css/bootstrap-rtl.css')}}" rel="stylesheet">
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap2-toggle.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm @lang('admin.dir')">
            <div class="container">
                <div class="navbar-brand">
                <!-- navbar toggler  -->
                <div class="btn btn-dark fa fa-bars" id="toggle-sidebar" onclick="document.getElementById('nav').classList.add('toggled');"></div>
                </div>
                <div class="right" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                                <div class="right" aria-labelledby="navbarDropdown">
                                <span>
                                <a href="/locale/en" class="btn"><img src="{{ asset ('lang/English.jpg') }}" class="image">English</a>
                                <a href="/locale/ar" class="btn"><img src="{{ asset ('lang/Arabic.png') }}" class="image">Arabic</a>
                                </span>
                                    <a  class="btn btn-primary" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                       @lang('admin.logout')
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                             </ul>
                        </div>
                    </div>
                </nav>

        <div class="container @lang('admin.dir')">
        <div class="row justify-content-center">
        <div class="col-md-8">
            <br>
                    @include('inc.message')
                    @yield('content')
         </div>
         </div>
        </div>   
        <div class="row">
        @include('inc.admin_nav')
        </div> 

</body>
</html>
