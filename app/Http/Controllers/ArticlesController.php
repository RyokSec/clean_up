<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use APP\User;
use App\Permissions;
use App\Articles;
use Auth;
use App;
use Illuminate\Support\Facades\DB;

class ArticlesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');     
    }

    public function index()
    {
        //check permissions
            if (!@Auth::user()->is_admin){
            return redirect('/user');
        }
        //check view permission and permission route states
        if(@Auth::user()->permissionsGroup->view_status == 'on' && @Auth::user()->permissionsGroup->clothes_status == 'on'){
        $title = 'Articles';
        $title_ar = 'قطع الملابس';
        $articles = Articles::all();
        return view('Admin.Articles.index', compact('title' , 'title_ar' , 'articles'));
        }
        abort(403, 'Unauthorized action.');
    }

    public function create()
    {
        //check permissions
        if (!@Auth::user()->is_admin){
        return redirect('/user');
        }
        //check view permission and permission route states
        if(@Auth::user()->permissionsGroup->add_status == 'on' && @Auth::user()->permissionsGroup->clothes_status == 'on'){
        $title_ar = 'اضافة قطعة جديدة';
        $title = 'Add Article';
        return view('Admin.Articles.create', compact('title','title_ar'));
        }
       abort(403, 'Unauthorized action.');
    }

    public function store(Request $request)
    {
                //check permissions
                if (!@Auth::user()->is_admin){
                    return redirect('/user');
                }
                //check view permission and permission route states
                if(@Auth::user()->permissionsGroup->view_status == 'on' && @Auth::user()->permissionsGroup->add_status == 'on'){
                   //validator
                    $request->validate([
                        'article_name' => 'required',
                        'article_name_ar' => 'required',
                        'icon' => 'required',
                    ]);
                    //store process
                    $article = new Articles ;
                    $article->article_name = $request->input('article_name');
                    $article->article_name_ar = $request->input('article_name_ar');
                    $article->article_icon = $request->icon;
                    //check if icon has been uploaded by user
                    if($article->article_icon == 'image'){
                        $imageName = time().'.'.request()->image->getClientOriginalExtension();
                        $image_path = '/Articles/uploaded_images/'.$imageName;
                        request()->image->move(public_path('/Articles/uploaded_images'), $imageName); 
                        $article->article_icon = $image_path;
                        $article->save();
                        return redirect('/admin/articles')->with('success','Article added successfully');
                    }
                    //or if icon choosed from the provided list
                    else{
                        $article->article_icon = $request->icon.'.png';
                        $article->save();
                        return redirect('/admin/articles')->with('success','Article added successfully');
                    }   
                }
                abort(403, 'Unauthorized action.');
    }
}
