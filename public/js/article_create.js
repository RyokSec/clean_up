function get_filename(){
    var file_name = document.getElementById("image").files[0].name;
    document.getElementById('pic_label').innerHTML = file_name;
    console.log(file_name);
}
function get_selected_radio() {
    if(document.getElementById("image_radio").checked){
        document.getElementById('file_input').classList.add("visible");
        document.getElementById('file_input').classList.remove("invisible");
    }else{
        document.getElementById('file_input').classList.add("invisible");
    }
}