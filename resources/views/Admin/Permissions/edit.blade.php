@extends('layouts.admin')
@section('content')
<br>
{!! Form::model($data,['method' => 'PATCH','action'=>['PermissionsController@update','id'=>$data->id]]) !!}
@csrf
    <div class="form-group">
    <label for="title" class="h4">@lang('admin.permission_title')</label>
    <input type="text" id="title" class="form-control" placeholder="@lang('admin.permission_title')" name="title" value="{{$data->title}}">
    <br>
    <label for="title_ar" class="h4">@lang('admin.permission_title_ar')</label>
    <input type="text" id="title_ar" class="form-control" placeholder="@lang('admin.permission_title_ar')" name="title_ar" value="{{$data->title_ar}}">
  </div>
  <table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">@lang('admin.permission')</th>
      <th scope="col">@lang('admin.state')</th>
    </tr>
  </thead>
  <tbody>
  @if($data->view_status == 'on')
    <tr>
    <td><label class="checkbox-inline">@lang('admin.view')</label></td>
     <td><input type="checkbox" checked data-toggle="toggle" name="view"></td>
    </tr>
  @else
  <tr>
  <td><label class="checkbox-inline">@lang('admin.view')</label></td>
     <td><input type="checkbox"  data-toggle="toggle" name="view"></td>
    </tr>
  @endif
  @if($data->view_status == 'on')
    <tr>
        <td><label class="checkbox-inline">@lang('admin.edit')</label></td>
        <td><input type="checkbox" checked data-toggle="toggle" name="edit"> </td>
    </tr>
  @else
  <tr>
  <td><label class="checkbox-inline">@lang('admin.edit')</label></td>
        <td><input type="checkbox"  data-toggle="toggle" name="edit"> </td>
    </tr>
  @endif
  @if($data->add_status == 'on')
    <tr>
    <td><label class="checkbox-inline">@lang('admin.add')</label></td>
        <td><input type="checkbox" checked data-toggle="toggle" name="add"> </td>
    </tr>
  @else
  <tr>
  <td><label class="checkbox-inline">@lang('admin.add')</label></td>
        <td><input type="checkbox"  data-toggle="toggle" name="add"> </td>
    </tr>
  @endif
  @if($data->delete_status == 'on')
     <tr>
     <td><label class="checkbox-inline">@lang('admin.delete')</label></td>
        <td><input type="checkbox" checked data-toggle="toggle" name="delete"></td>
    </tr>
    @else
    <tr>
    <td><label class="checkbox-inline">@lang('admin.delete')</label></td>
        <td><input type="checkbox" data-toggle="toggle" name="delete"></td>
    </tr>
    @endif
    @if($data->permissions_status == 'on')
    <tr>
    <td><label class="checkbox-inline">@lang('admin.permission_access')</label></td>
        <td><input type="checkbox" checked data-toggle="toggle" name="permissions"></td>
    </tr>
    @else
    <tr>
    <td><label class="checkbox-inline">@lang('admin.permission_access')</label></td>
        <td><input type="checkbox" data-toggle="toggle" name="permissions"></td>
    </tr>
    @endif
    @if($data->analytics_status == 'on')
    <tr>
    <td><label class="checkbox-inline">@lang('admin.analytics_access')</label></td>
        <td><input type="checkbox" checked data-toggle="toggle" name="analytics"></td>
    </tr>
    @else
    <tr>
    <td><label class="checkbox-inline">@lang('admin.analytics_access')</label></td>
        <td><input type="checkbox"  data-toggle="toggle" name="analytics"></td>
    </tr>
    @endif
    @if($data->services_status == 'on')
    <tr>
    <td><label class="checkbox-inline">@lang('admin.services_access')</label></td>
        <td><input type="checkbox" checked data-toggle="toggle" name="services"></td>
    </tr>
    @else
    <tr>
    <td><label class="checkbox-inline">@lang('admin.services_access')</label></td>
        <td><input type="checkbox" data-toggle="toggle" name="services"></td>
    </tr>
    @endif
    @if($data->clothes_status == 'on')
    <tr>
    <td><label class="checkbox-inline">@lang('admin.clothes_access')</label></td>
        <td><input type="checkbox" checked data-toggle="toggle" name="clothes"></td>
    </tr>
    @else
    <tr>
    <td><label class="checkbox-inline">@lang('admin.clothes_access')</label></td>
        <td><input type="checkbox"  data-toggle="toggle" name="clothes"></td>
    </tr>
    @endif
    @if($data->storage_status == 'on')
    <tr>
    <td><label class="checkbox-inline">@lang('admin.storage_access')</label></td>
        <td><input type="checkbox" checked data-toggle="toggle" name="storage"></td>
    </tr>
    @else
    <tr>
    <td><label class="checkbox-inline">@lang('admin.storage_access')</label></td>
        <td><input type="checkbox" data-toggle="toggle" name="storage"></td>
    </tr>
    @endif  
  </tbody>
</table>
  <br>
  <div>
  {{Form::submit(trans('admin.submit'),['class' => 'btn btn-primary'])}}
</div>
  @endsection

