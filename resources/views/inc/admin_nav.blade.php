<div class="page-wrapper chiller-theme sidebar toggled wrapper @lang('admin.dir')" id="nav">
        <nav id="sidebar" class="sidebar-wrapper">
            <div class="sidebar-content">
                <div class="sidebar-brand text-center">
                <!-- button toggler -->
                 <div class="btn btn-dark fa fa-bars" id="collapse-sidebar" onclick="collapse_nav()"></div>
                    <a href="/admin">Clean up</a>
                </div>
                <div class="sidebar-header navbar-brand">
                    <div class="user-pic">
                        <img class="img-responsive img-rounded" src=" {{Auth::user()->image}}" alt="User picture">
                    </div>
                    <div class="user-info">
                        <span class="user-name">
                            <strong>
                            {{Auth::user()->name}}                          
                            </strong>
                        </span>
                        @if( Config::get('app.locale') == 'en')
                        <span class="user-role">{{ Auth::user()->PermissionsGroup->title }}</span>
                        @else
                        <span class="user-role">{{ Auth::user()->PermissionsGroup->title_ar }}</span>
                        @endif
                    </div>
                </div>
                <!-- sidebar-header  -->
                <div class="sidebar-search">
                    <div>
                        <div class="input-group">
                            <input type="text" class="form-control search-menu">
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- sidebar-search  -->
                <div class="sidebar-menu">
                    <ul>
                        <li class="header-menu">
                            <span>@lang('admin.general')</span>
                        </li>
                        <li class="nav-item sidebar">
                            <a href="#">
                                <i class="fa fa-tachometer"></i>
                                <span>@lang('admin.dashboard')</span>
                            </a>
                            
                        <li class="nav-item sidebar">
                            <a href="#">
                                <i class="fa fa-shopping-cart"></i>
                                <span>@lang('admin.services')</span>
                            </a>
                           
                        <li class="nav-item sidebar">
                            <a href="#">
                                <i class="fa fa-bar-chart"></i>
                                <span>@lang('admin.analytics')</span>
                            </a>
                           
                        <li class="nav-item sidebar">
                            <li class="nav-item sidebar">
                            <a href="/admin/permissions">
                                <i class="fa fa-id-card"></i>
                                <span>@lang('admin.permissions')</span>
                            </a>
                            <li class="nav-item sidebar">
                            <a href="/admin/articles">
                                <i class="fa fa-black-tie "></i>
                                <span>@lang('admin.articles')</span>
                            </a>
                            <li class="nav-item sidebar">
                            <a href="#">
                                <i class="fa fa-user"></i>
                                <span>@lang('admin.profile')</span>
                            </a>
                            <li class="nav-item sidebar">
                            <a href="#">
                                <i class="fa fa-cogs"></i>
                                <span>@lang('admin.general_settings')</span>
                            </a>
                                </ul>
                            </div>
                        </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
                <!-- sidebar-menu  -->
            </div>
            <!-- sidebar-content  -->
        </nav>
        <!-- sidebar-wrapper  -->
        </div>