<?php

namespace App\Http\Controllers;

use Auth;
use Helper;
use Redirect;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');     
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    public function no_permission()
    {
        return view('Http_errors.401');
    }



    public function Dashboard()
    {
        if (!@Auth::user()->is_admin){
        return redirect('/user');
    }
        $title = 'Dashboard';
        $title_ar = 'لوحة التحكم';
        return view('Admin.Dashboard',compact('title' , 'title_ar'));
    }
}
